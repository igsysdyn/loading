from django.conf import settings
from minio import Minio
from minio.error import S3Error

from loading.celery_settings import app
from .decorators import single_instance_task


DEFAULT_DIRECTORY_NAME = "default"
MAX_RETRIES = 5
TIMEOUT = 60 * 5


@app.task(name='loading.tasks.upload_to_cloud', auto_retry=(S3Error,),
          max_retries=MAX_RETRIES, retry_backoff=True)
@single_instance_task(TIMEOUT)
def upload_to_cloud(filename, path):
    """Uploads file to the cloud.

    :param filename: Filename
    :param path: Path to file
    """
    client = Minio(
        settings.MINIO_HOST,
        access_key=settings.MINIO_ACCESS_KEY,
        secret_key=settings.MINIO_SECRET_KEY,
        secure=False
    )
    found = client.bucket_exists(DEFAULT_DIRECTORY_NAME)
    if not found:
        client.make_bucket(DEFAULT_DIRECTORY_NAME)

    client.fput_object(DEFAULT_DIRECTORY_NAME, filename, path)
