import os
from http import HTTPStatus

from django.conf import settings
from django.http import JsonResponse
from django.views import View

from .services import get_cache_by_key, get_md5, set_cache_by_key
from .tasks import upload_to_cloud


MEDIA_ROOT = settings.MEDIA_ROOT


class UploadFile(View):

    def post(self, request, *args, **kwargs):
        """Checks and saves file on the server."""
        headers = request.headers
        if 'x-file-id' not in headers:
            return JsonResponse({'msg': 'x-file-id not found'},
                                status=HTTPStatus.BAD_REQUEST)
        if 'x-file-hash' not in headers:
            return JsonResponse({'msg': 'x-file-hash not found'},
                                status=HTTPStatus.BAD_REQUEST)
        content_length = int(headers.get('content-length', 0))
        if not content_length:
            return JsonResponse({'msg': 'content not found'},
                                status=HTTPStatus.BAD_REQUEST)
        start_byte = int(headers.get('x-start-byte', 0))
        file_size = get_cache_by_key(headers['x-file-id'], 0)
        file_hash = headers['x-file-hash']
        file_info = headers['x-file-id'].split('-')
        new_size = file_size + content_length
        if new_size > int(file_info[1]):
            return JsonResponse({'msg': 'file is full'}, status=HTTPStatus.BAD_REQUEST)
        if start_byte:
            if start_byte > file_size:
                return JsonResponse({'msg': 'x-start-byte doesn\'t fit file size'},
                                    status=HTTPStatus.BAD_REQUEST)
            mode = 'a'
        else:
            mode = 'w'

        path = os.path.join(MEDIA_ROOT, file_info[0])
        with open(path, f"{mode}b+") as file:
            file.write(request.body)
            if new_size == int(file_info[1]):
                hash_md5 = get_md5(file)
                if file_hash != hash_md5:
                    return JsonResponse({'msg': 'Checksum doesn\'t fit. Restart loading.'},
                                        status=HTTPStatus.BAD_REQUEST)

                upload_to_cloud.delay(file_info[0], path)
        set_cache_by_key(headers['x-file-id'], new_size)
        return JsonResponse({'msg': 'Uploaded'})

    def get(self, request, *args, **kwargs):
        """Gets uploaded bytes of file."""
        headers = request.headers
        if 'x-file-id' not in headers:
            return JsonResponse({'msg': 'x-file-id not found'},
                                status=HTTPStatus.BAD_REQUEST)
        file_size = get_cache_by_key(headers['x-file-id'], 0)
        return JsonResponse({'bytes': file_size})
