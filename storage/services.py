import hashlib

from django.core.cache import cache


def get_cache_by_key(key, default=None):
    """Returns value from cache by key or default value.

    :param key: Key
    :param default: Default value
    :return: Value or default
    """
    return cache.get(key) or default


def set_cache_by_key(key, value):
    """Sets value to cache by key.

    :param key: Key
    :param value: Value
    """
    cache.set(key, value)


def get_md5(file):
    """Returns checksum of file.

    :param file: File descriptor
    :return: Checksum
    """
    hash_md5 = hashlib.md5()
    for chunk in iter(lambda: file.read(4096), b""):
        hash_md5.update(chunk)
    return hash_md5.hexdigest()
